to run:
npm install
npm start

notes:
this is a demonstration of code splitting and express rendering the react components

retains:
react-dom/server rendertostring

improvements:
add redux for async api calls and state update
react router match with server routes
react router context
