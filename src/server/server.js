import Express from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'
import Home from '../common/component/appComponent'
import Users from '../common/component/users-component'


const PORT = 3001
const app = Express()
// serve dist folder as static asset
app.use('/dist', Express.static('dist'))

// this is just proof of concept to demonstrate
// server side rendering
app.get('/',(req,res)=>{

let initialState = {data:"Home Page",time: new Date().toLocaleTimeString()}
let mainComponent = renderToString(<Home {...initialState} />)
let htmlString = `<!DOCTYPE html>
<html>
<head>
  <title>
  Basic Isomorphic react
  </title>
  <script>window.__APP_INITIAL_STATE__ = ${JSON.stringify(initialState)}</script>
  <link href="../common/component/todo-app.css" />
</head>
<body>
  <div id="reactDiv"/>${mainComponent}<div>
  <script src="/dist/home.js"></script>
</body>
</html>`

  res.end(htmlString)
})

app.get('/users',(req,res)=>{
let initialState = {data:"Users Page", time:new Date().toLocaleTimeString()}
let mainComponent = renderToString(<Users {...initialState} />)

let htmlString = `<!DOCTYPE html>
<html>
<head>
  <title>
  Basic Isomorphic react
  </title>
  <script>window.__APP_INITIAL_STATE__ = ${JSON.stringify(initialState)}</script>
  <link href="../common/component/todo-app.css" />
</head>
<body>
  <div id="reactDiv"/>${mainComponent}<div>
  <script src="/dist/home.js"></script>
</body>
</html>`

  res.end(htmlString)
})
app.get('*',(req,res)=>{
  res.end('file not found 404')
})
app.listen(PORT, ()=>{
  console.log(`SERVER: listening at ${PORT}`)
})
