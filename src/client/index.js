
// mount point
// this app is not using react-dom/server (reactDOMServer)
import React from 'react'
import {render} from 'react-dom'
import App from '../common/component/appComponent'
import  '../common/component/todo-app.css'

render(<App {...window.__APP_INITIAL_STATE__ } />, document.getElementById('reactDiv'))
