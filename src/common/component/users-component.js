
import React, {Component} from 'react'
import TodoApp from './todo-app'
export default class App extends Component{

  constructor(props){
    super(props)
    this.state = {
      timeNow: new Date().toLocaleTimeString()
    }
    this.setTime = this.setTime.bind(this)
  }
  componentDidMount() {
    this.timerId = setInterval(this.setTime, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timerId);
  }
  setTime() {
    this.setState({timeNow: (new Date()).toLocaleTimeString()});
  }
  render(){
    let {data} = this.props
    return(
      <div>
        <h1>{data}</h1>
        <p>the time is : {this.state.timeNow}</p>
        <TodoApp />
      </div>
    )
  }
}
/*http://infoheap.com/react-component-clock-using-setinterval/
getInitialState: function(){
    return {date:  (new Date()).toLocaleTimeString()};
  },
  componentDidMount: function() {
    this.timerId = setInterval(this.update_time, 1000);
  },
  update_time: function() {
    this.setState({date: (new Date()).toLocaleTimeString()});
  },
  render: function() {
    return (<div>{this.state.date}</div>);
  },
  componentWillUnmount: function() {
    clearInterval(this.timerId);
  },
*/
