import React, { Component } from 'react';
// import { Router,
//   Route,
//   Link,
//   IndexRoute, // default route
//   //hashHistory,
//   browserHistory
//   } from 'react-router'
//
// import axios from 'axios'
// import logo from './logo.svg';
// import './App.css';
// import FirebaseAuth from './firebase-auth'

// https://www.npmjs.com/package/jsxstyle

// add feature: onclick todo item clear textbox and update the todo text
const TodoItem = (props)=>{
  return(
    <div value="teststststststt">
        <input type="checkbox" value={props.data.key} checked={props.data.isDone} onClick={props.didTodo}/>
        <li value="testssss">{props.data.todo}</li>
        <button  value={props.data.key} onClick={props.removeTodo}>X</button>
      </div>

  )
}

// https://facebook.github.io/react/docs/lists-and-keys.html
// this component is rerendered every time state is updated by the input form
const TodoList = (props)=>{
  const retList = props.list.map((item)=>{
    console.log('%% todo item',item)
    return(
      <TodoItem key={item.key}   data={item} removeTodo={props.deleteItem} didTodo={props.didTodo}/>
    )
  })
  // console.log('%%% retlist',retList)
  return(
    <div className="theList">
      {retList}
    </div>
  )
}

// https://facebook.github.io/react/docs/forms.html
const TodoForm = (props)=>{
  // console.log('form rendered due to state change')
  return(
    <form onSubmit={props.addItem}>
        {props.children}
    </form>
  )
}

// has a todolist and todoform components
class TodoApp extends Component{
  constructor(props){
    super(props)
    this.state= {
        items:[],
        inputText: ''
    };
    // bind function to this class
    this.getInput = this.getInput.bind(this)
    this.addItem = this.addItem.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
    this.didTodo = this.didTodo.bind(this)
  }

  getInput(event){
    this.setState({inputText : event.target.value})
  }

  addItem(event){
    const getItems = this.state.items
    const newdata = {
      // refs are going away so use event.target.value
      // refs are used to do two-way data binding
      // todo: this.inputTodoNode.value,
      todo: this.state.inputText,
      key: Date.now(),
      isDone : false
    }
    getItems.push(newdata)
    this.setState({
      items: getItems
    })
    this.inputTodoNode.value = ''
    this.setState({inputText:''})
    // console.log(event.target.value)
    console.log('%%% value from refs',this.inputTodoNode.value)
    console.log('%%% update state',this.state)
    event.preventDefault()
  }

  didTodo(event){
    let getItems = this.state.items
    // console.log('%%% todo ref value', event.target.checked)
    // console.log('%%% todo ref index', event.target.value)
    let isDone = event.target.checked
    let todoKey = event.target.value
    // let index = getItems.findIndex(n=>n.key.toString() === todoKey)
    getItems.map((item)=>{
      if(item.key.toString() === todoKey){
        item.isDone = isDone
      }
    })
    // not necessary to reset state
    this.setState({})
  }

  deleteItem(event){
    const getItems = this.state.items
    // console.log('%%% event',event)
    // console.log('%%% todo item',event.target)
    // console.log('%%% todo ref value', event.target.value)
    let todoKey = event.target.value
    let index = getItems.findIndex(i=>i.key.toString() === todoKey)
    // console.log('%%% index of key', index)
    // for(let i =0;i<getItems.length;i++){
    getItems.map((item)=>{
        if(item.key.toString() === todoKey){
          console.log('found',item)
          //splice the state array and reset state
          getItems.splice(index,1)
          console.log('%%% new state',getItems)
        }
    })
    this.setState({
      items: getItems
    })
  }

  render(){
    return(
      <div className="todoListMain">
        <div className="header">
          <TodoForm addItem={this.addItem} >
            <input placeholder="enter todo task" value={this.state.inputText}
            onChange={this.getInput} ref={(a)=>this.inputTodoNode=a}>
              </input>
            <button type="submit" >Add</button>
          </TodoForm>
          <TodoList list={this.state.items} deleteItem={this.deleteItem} didTodo={this.didTodo}/>
        </div>
      </div>
    )
  }
}
export default TodoApp
