var webpack = require('webpack')
var path = require('path')
//var ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  devtool: 'eval-source-map',
  // entry: path.resolve(__dirname, 'src/client/index'),
  entry:{
    home: path.resolve(__dirname, 'src/client/index'),
    users: path.resolve(__dirname, 'src/client/users')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    // filename: 'bundle.js'
    filename:"[name].js",
    chunkFilename: "[id].js"
  },
  module:{
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      include: path.resolve(__dirname, 'src')
    },{
        test: /\.css$/,
        loader: ['style-loader','css-loader']
      }]
  }
  // TODO extract-text-webpack-plugin
}
